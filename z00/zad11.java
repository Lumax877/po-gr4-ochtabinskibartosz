public class zad11 {
    public static void main(String[] args) {
        String wiersz = " Samotność," +
                "\n ogromna perła samotności," +
                "\n w niej się ukryłam." +
                "\n Przestrzeń," +
                "\n która się rozprzestrzenia," +
                "\n w niej ogromnieję." +
                "\n Cisza, źródło głosów." +
                "\n Nieruchomość, matka ruchu." +
                "\n Jestem sama." +
                "\n Jestem sama, a więc jestem niczym." +
                "\n Co za szczęście." +
                "\n Jestem niczym, a więc mogę być wszystkim." +
                "\n Egzystencja bez esencji," +
                "\n esencja bez egzystencji, wolność.";

        System.out.println(wiersz);
    }
}
