
import java.util.Scanner;

public class zad2d {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj n");
        int n = in.nextInt();
        System.out.println("Podawaj liczby naturalne");
        float[] liczby = new float[n];
        int wynik = 0;
        for(int i = 0; i < n; i++){
            liczby[i] = in.nextFloat();
            if(i != 0 && i != n - 1)
                if(liczby[i] < (liczby[i-1] + liczby[i+1])/2)
                    wynik++;
        }
        System.out.println("Wynik: "+ wynik);
        in.close();
    }
}

