import java.util.Scanner;


public class zad2c{
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj n");
        int n = in.nextInt();
        System.out.println("Podawaj liczby naturalne");
        float[] liczby = new float[n];
        int wynik = 0;
        for(int i = 0; i < n; i++){
            liczby[i] = in.nextFloat();
            double pierw = Math.sqrt(liczby[i]);
            if((pierw - Math.floor(pierw)) == 0 && pierw % 2 == 0){
                wynik += 1;
            }
        }
        System.out.println("Wynik: "+ wynik);
        in.close();
    }
}