import java.util.Scanner;

public class zad1b {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj n: ");
        int n = scan.nextInt();
        scan.close();
        int fact = 1;
        for (int i = 2; i <= n; i++){
            fact = fact * i;
        }
        System.out.println(fact);
    }
}
