import java.util.ArrayList;
import java.util.Arrays;

public class tak{

    public static ArrayList<Integer> reversed(ArrayList<Integer> a){
        int temp = 0;
        for(int i = 0; i <= a.size() / 2; i++){
            temp = a.get(i);
            a.set(i,a.get(a.size() - 1 - i));
            a.set(a.size() - 1 - i, temp);
        }
        return a;
    }
    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<>(Arrays.asList(3,8,5,7,1));
        System.out.print(reversed(a));

    }
}